﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Assignment_1a_rewrite.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Booking:</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>  
         <h1>Hotel Booking Form:</h1>
         <label>First Name:</label><br/>
         <asp:TextBox runat="server" ID="customerFirstName"></asp:TextBox>
         <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Fill Out Your FirstName" ControlToValidate="customerFirstName" ID="VerifyName"></asp:RequiredFieldValidator>
         <br/>
         <label>Last Name:</label><br/>
        <asp:TextBox runat="server" ID="customerLastName"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Fill Out Your LastName" ControlToValidate="customerLastName" ID="LastNameValidator"></asp:RequiredFieldValidator>
       <br/>
       <label>Address:</label><br/>
       <asp:TextBox runat="server" ID="customerAddress" PlaceHolder="Enter Address"></asp:TextBox>
       <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Fill Out Your Address" ControlToValidate="customerAddress" ID="AddressValidator"></asp:RequiredFieldValidator>
      <br/>
       <label>PostalCode:</label><br/>
       <asp:TextBox runat="server" ID="customerPostalCode" PlaceHolder="N2E4V4"></asp:TextBox>
       <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Fill Out PostalCode" ControlToValidate="customerPostalCode" ID="VerifyCode"></asp:RequiredFieldValidator>
       <br/><br/>
        </div>
         <div>
         <label>Guest:</label>
         <asp:DropDownList runat="server" ID="numberofGuest">
         <asp:ListItem Text="ONE" Value="1"></asp:ListItem>
         <asp:ListItem Text="TWO" Value="2"></asp:ListItem>
         <asp:ListItem Text="THREE" Value="3"></asp:ListItem>
         </asp:DropDownList><br /><br/>
         <label>Children:</label>
         <asp:DropDownList runat="server" ID="numberofChild">
         <asp:ListItem Text="ONE" Value="1"></asp:ListItem>
         <asp:ListItem Text="TWO" Value="2"></asp:ListItem>
         </asp:DropDownList>
         </div>
         <br/>
        <label>Suite :</label>
        <asp:RadioButton runat="server" ID="roomPriorityCouple" Text="Double" GroupName="typeofRoom"></asp:RadioButton>
        <asp:RadioButton runat="server" ID="roomPriorityFamily" Text="King" GroupName="typeofRoom"></asp:RadioButton>
        <br /><br />
         <div id="extraFacilities" runat="server">
         <label>Facilities:</label>
         <asp:ChecKBox runat="server" ID="clientChoice1" Text="SwimmingPool" />
         <asp:ChecKBox runat="server" ID="clientChoice2" Text="Lunch" />
         <asp:ChecKBox runat="server" ID="clientChoice3" Text="Internet"/>
         </div>
         <br /><br/>
        <div>
        <label>Booking Day:</label>
        <asp:TextBox runat="server" ID="bookingDays"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your Reservation Day" ControlToValidate="BookingDays" ID="dayValidator"></asp:RequiredFieldValidator>
        <asp:RangeValidator runat="server" ControlToValidate="BookingDays" Type="Integer" MinimumValue="1" MaximumValue="3" ErrorMessage="Booking Only Between 1 to 3 Days"></asp:RangeValidator>
         </div>
        <br/>
        <div>
        <label>CheckInTime:</label>
        <asp:TextBox runat="server" ID="CheckInTime"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter checkin Time" ControlToValidate="CheckInTime" ID="Timevalidator"></asp:RequiredFieldValidator>
        <asp:RangeValidator runat="server" ControlToValidate="CheckInTime" Type="Integer" MinimumValue="4" MaximumValue="10" ErrorMessage="Time should be 4pm to 10pm"></asp:RangeValidator>
        </div>
        <br/><br/>
        <label>Email ID :</label>
        <asp:TextBox runat="server" ID="customerEmail" placeHolder="abc@xy.com"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Fill Out Email ID" ControlToValidate="customerEmail" ID="verifyEmail"></asp:RequiredFieldValidator>
         
        <%--Emailvalidation expression reference from class example --%>
         
        <asp:RegularExpressionValidator runat="server" ID="validateEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
           ControlToValidate="customerEmail" ErrorMessage="Please Enter Valid Email"></asp:RegularExpressionValidator>
        <br/><br />
        <label>Contact No:</label>
        <asp:TextBox runat="server" ID="customerContactNumber" ></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Fill Your Contact Number" ControlToValidate="customerContactNumber" ID="VerifyContactNumber"></asp:RequiredFieldValidator>
        <asp:CompareValidator runat="server" ControlToValidate="customerContactNumber" Type="Integer" Operator="DataTypeCheck" ErrorMessage="Contact Number Must be whole number" ></asp:CompareValidator>
        <br/><br/>
        <label>Best Match:</label>
        <asp:RadioButton runat="server" ID="Match" Text="LowestPrice" GroupName="priority"></asp:RadioButton>
        <asp:RadioButton runat="server" ID="Best"   Text="HighRating" GroupName="priority"></asp:RadioButton>
        <br/><br/>
        <asp:Button runat="server" ID="submitButton" onclick="BookRoom" Text="Submit" /> 

        <div runat="server" id="BookRoomSli">

        </div>


    </form>
</body>
</html>
