﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_1a_rewrite
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }   
        protected void BookRoom (object sender,EventArgs e)
        {  
           

            int bookingday = int.Parse(bookingDays.Text);
            string guest = numberofGuest.SelectedItem.Value.ToString();
            List<string>  facilities = new List<string> { "Parking"};
            string children = numberofChild.SelectedItem.Value.ToString();
            RoomType newroomtype = new RoomType (bookingday, guest, children, facilities);


            string firstname = customerFirstName.Text.ToString();
            string lastname = customerLastName.Text.ToString();
            string address = customerAddress.Text.ToString();
            string postalcode = customerPostalCode.Text.ToString();
            string email = customerEmail.Text.ToString();
            int contactnumber = int.Parse(customerContactNumber.Text);

            Client newclient = new Client();
            newclient.ClientFirstName = firstname;
            newclient.ClientLastName = lastname;
            newclient.ClientAddress = address;
            newclient.ClientPostalCode = postalcode;
            newclient.ClientEmail = email;
            newclient.ClientContactNumber = contactnumber;


            string name = customerFirstName.Text.ToString();
            int time = int.Parse(CheckInTime.Text);

            CheckInTime newcheckintime = new CheckInTime(time,name);
           
            /* REFRENCE FROM CLASS EXAMPLE */

            List<string> facilitiesvalue = new List<string>();
            foreach (Control control in extraFacilities.Controls)
            {

                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox cbfacilities = (CheckBox)control;

                    if (cbfacilities.Checked)
                    {
                        facilitiesvalue.Add(cbfacilities.Text);
                    }
                }
            }
            newroomtype.facilities = newroomtype.facilities.Concat(facilitiesvalue).ToList();



            BookRoom newbookRoom = new BookRoom(newroomtype, newclient, newcheckintime);

            BookRoomSli.InnerHtml = newbookRoom.BookingSlip();



        }
       
        
          
        
        
    }
}